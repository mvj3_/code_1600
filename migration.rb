# encoding: UTF-8

# 去除外键，否则会报Mysql2::Error: Cannot drop index 'index_learn_price_on_course_id': needed in a foreign key constraint: DROP INDEX `index_learn_price_on_course_id` ON `learn_price`错误
execute "ALTER TABLE learn_price DROP FOREIGN KEY learn_price_ibfk_1" rescue nil

[:learn_course, :learn_price, :learn_lesson, :learn_section].each do |table|
  ActiveRecord::Base.connection.indexes(table).each do |index|
    remove_index table, :name => index.name
  end
end